const express = require("express");
const router = express.Router();
const Support = require("../models/support"); //import Post models

router.get("/", async (req, res) => {
  try {
    const get = await Support.find();
    res.json(get);
  } catch (err) {
    res.json({ message_posts_get: err });
  }
});

router.post("/", async (req, res) => {
  const iterable = req.body.supporttwo[0].tabel_data;

  const post = new Support({
    supportone: [
      {
        id_file: req.body.supportone[0].id_file,
        date_file: req.body.supportone[0].date_file,
        moneysupport: req.body.supportone[0].moneysupport,
        supportpeople: req.body.supportone[0].supportpeople,
        necessity: req.body.supportone[0].necessity,
        goal_project: req.body.supportone[0].goal_project,
        manage: req.body.supportone[0].manage,
        phase_manage: req.body.supportone[0].phase_manage,
        details: req.body.supportone[0].details,
        consider_media: [
          {
            media: [
              {
                media_check:
                  req.body.supportone[0].consider_media[0].media[0].media_check,
                media_date:
                  req.body.supportone[0].consider_media[0].media[0].media_date,
                media_since:
                  req.body.supportone[0].consider_media[0].media[0].media_since,
              },
            ],
            sing_presenter:
              req.body.supportone[0].consider_media[0].sing_presenter,
            date_presenter:
              req.body.supportone[0].consider_media[0].date_presenter,
          },
        ],
        budget: [
          {
            budget: [
              {
                total: req.body.supportone[0].budget[0].budget[0].total,
                txt_total: req.body.supportone[0].budget[0].budget[0].txt_total,
              },
            ],
            borrow_budget: [
              {
                setback:
                  req.body.supportone[0].budget[0].borrow_budget[0].setback,
                total: req.body.supportone[0].budget[0].borrow_budget[0].total,
                txt_total:
                  req.body.supportone[0].budget[0].borrow_budget[0].txt_total,
              },
            ],
            my_support: [
              {
                total: req.body.supportone[0].budget[0].my_support[0].total,
                txt_total:
                  req.body.supportone[0].budget[0].my_support[0].txt_total,
              },
            ],
            sum_total: req.body.supportone[0].budget[0].sum_total,
            sum_txt_total: req.body.supportone[0].budget[0].sum_txt_total,
          },
        ],
        result_project: req.body.supportone[0].result_project,
        sing_presenter: req.body.supportone[0].sing_presenter,
        confirm_presenter: req.body.supportone[0].confirm_presenter,
        date_presenter: req.body.supportone[0].date_presenter,
        support_money_day: req.body.supportone[0].support_money_day,
        support_money_time: req.body.supportone[0].support_money_time,
        consider: [
          {
            consider: [
              {
                consider_check:
                  req.body.supportone[0].consider[0].consider[0].consider_check,
                consider_date:
                  req.body.supportone[0].consider[0].consider[0].consider_date,
                consider_since:
                  req.body.supportone[0].consider[0].consider[0].consider_since,
              },
            ],
            sing_presenter: req.body.supportone[0].consider[0].sing_presenter,
            confirm_presenter:
              req.body.supportone[0].consider[0].confirm_presenter,
            date_presenter: req.body.supportone[0].consider[0].date_presenter,
          },
        ],
        monney: [
          {
            date_payee: req.body.supportone[0].monney[0].date_payee,
            time_payee: req.body.supportone[0].monney[0].time_payee,
            money: [
              {
                money_check:
                  req.body.supportone[0].monney[0].money[0].money_check,
                money_bankok:
                  req.body.supportone[0].monney[0].money[0].money_bankok,
                name_bankok:
                  req.body.supportone[0].monney[0].money[0].name_bankok,
                number_bankok:
                  req.body.supportone[0].monney[0].money[0].number_bankok,
              },
            ],
            sum_total: req.body.supportone[0].monney[0].sum_total,
            sum_txt_total: req.body.supportone[0].monney[0].sum_txt_total,
            note: req.body.supportone[0].monney[0].note,
            sing_payer: req.body.supportone[0].monney[0].sing_payer,
            sing_payee: req.body.supportone[0].monney[0].sing_payer,
            approve_date: req.body.supportone[0].monney[0].approve_date,
          },
        ],
      },
    ],
    supporttwo: [
      {
        nameproject: req.body.supporttwo[0].nameproject,
        tabel_data: iterable,
        total: req.body.supporttwo[0].total,
        txt_total: req.body.supporttwo[0].txt_total,
      },
    ],
  });
  try {
    const save = await post.save();
    res.json(save);
  } catch (error) {
    res.json({ message: err });
  }
});
module.exports = router;
