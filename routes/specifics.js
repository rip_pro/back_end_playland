const express = require("express");
const router = express.Router();
const Specific = require("../models/specific"); //import Post models

router.get("/", async (req, res) => {
  try {
    const get = await Specific.find();
    res.json(get);
  } catch (err) {
    res.json({ message_posts_get: err });
  }
});

router.post("/", async (req, res) => {
  const iterable = req.body.specifictwo[0].tabel_data;
  const post = new Specific({
    specificone: [
      {
        id_file: req.body.specificone[0].id_file,
        date_file: req.body.specificone[0].date_file,
        presenter: req.body.specificone[0].presenter,
        affiliation: req.body.specificone[0].affiliation,
        clarify_information: req.body.specificone[0].clarify_information,
        total: req.body.specificone[0].total,
        txt_total: req.body.specificone[0].txt_total,
        date_start: req.body.specificone[0].date_start,
        date_stop: req.body.specificone[0].date_stop,
        sum_total: req.body.specificone[0].sum_total,
        sum_total_txt: req.body.specificone[0].sum_total_txt,
        offer_date: req.body.specificone[0].offer_date,
        offer_time: req.body.specificone[0].offer_time,
        sing_presenter: req.body.specificone[0].sing_presenter,
        confirm_presenter: req.body.specificone[0].confirm_presenter,
        date_presenter: req.body.specificone[0].date_presenter,
        examine: [
          {
            examine_file: [
              {
                file_check:
                  req.body.specificone[0].examine[0].examine_file[0].file_check,
                file_date:
                  req.body.specificone[0].examine[0].examine_file[0].file_date,
                file_editfile:
                  req.body.specificone[0].examine[0].examine_file[0]
                    .file_editfile,
                file_txt_total:
                  req.body.specificone[0].examine[0].examine_file[0]
                    .file_txt_total,
              },
            ],
            note: req.body.specificone[0].examine[0].note,
            sing_inspector: req.body.specificone[0].examine[0].sing_inspector,
            date_inspector: req.body.specificone[0].examine[0].date_inspector,
            receive_money: req.body.specificone[0].examine[0].receive_money,
            receive_money_txt:
              req.body.specificone[0].examine[0].receive_money_txt,
            money: [
              {
                money_check:
                  req.body.specificone[0].examine[0].money[0].money_check,
                money_bankok:
                  req.body.specificone[0].examine[0].money[0].money_bankok,
                name_bankok:
                  req.body.specificone[0].examine[0].money[0].name_bankok,
                number_bankok:
                  req.body.specificone[0].examine[0].money[0].number_bankok,
              },
            ],
            money_note: req.body.specificone[0].examine[0].money_note,
            money_day: req.body.specificone[0].examine[0].money_day,
            money_time: req.body.specificone[0].examine[0].money_time,
            sing_money_inspector:
              req.body.specificone[0].examine[0].sing_money_inspector,
            date_money: req.body.specificone[0].examine[0].date_money,
            sing_payee: req.body.specificone[0].examine[0].sing_payee,
          },
        ],
      },
    ],
    specifictwo: [
      {
        id_file: req.body.specifictwo[0].id_file,
        date_file: req.body.specifictwo[0].date_file,
        presenter: req.body.specifictwo[0].presenter,
        affiliation: req.body.specifictwo[0].affiliation,
        tabel_data: iterable,
        total: req.body.specifictwo[0].total,
        txt_total: req.body.specifictwo[0].txt_total,
      },
    ],
  });
  try {
    const save = await post.save();
    res.json(save);
  } catch (error) {
    res.json({ message: err });
  }
});
module.exports = router;
