const express = require("express");
const router = express.Router();
// const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const JWT_SECRET =
  "sdjkfh8923yhjdksbfma@#*(&@*!^#&@bhjb2qiuhesdbhjdsfg839ujkdhfjk";

var test = [];
router.get("/meget", async (req, res) => {
  const { username, password } = req.body;
  try {
    const user = await User.find({ username }).lean();
    console.log("sds", res.json(user));
  } catch (err) {
    res.json({ message_posts_get: err });
  }
});

router.get("/me", verifyToken, (req, res) => {
  jwt.verify(req.token, "secretkey", (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json({
        message: "Post created...",
        authData,
      });
    }
  });
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;
  console.log("test username", username);

  const user = await User.findOne({ username }).lean();
  if (!user) {
    console.log("test user", user);

    return res.json({ status: "error", error: "Invalid username/password" });
  }
  if (await (password == user.password)) {
    jwt.sign({ user }, "secretkey", (err, token) => {
      res.json({
        user: {
          token,
          success: true,
          user,
        },
      });
    });
  }
  // res.json({ status: "error", error: "Invalid username/password" });

  // const { username, password } = req.body;
  // const user = await User.findOne({ username }).lean();
  // if (!user) {
  //   return res.json({ status: "error", error: "Invalid username/password" });
  // }

  // if (await bcrypt.compare(password, user.password)) {
  //   // the username, password combination is successful
  //   const token = jwt.sign(
  //     {
  //       id: user._id,
  //       username: user.username,
  //     },
  //     JWT_SECRET
  //   );
  //   // return res.json(console.log(" token", { status: "ok", data: token }));
  //   return res.json({ status: "ok", data: token });
  // }

  // res.json({ status: "error", error: "Invalid username/password" });
});

router.post("/register", async (req, res) => {
  console.log(req.body);
  //จะทำการปรับ password ให้อยู่ในรูปแบบ 45fasdf36asd4fasdfasdfasd
  // const { username, password: plainTextPassword } = req.body;
  // if (!username || typeof username !== "string") {
  //   return res.json({ status: "error", error: "Invalid username" });
  // }
  // if (!plainTextPassword || typeof plainTextPassword !== "string") {
  //   return res.json({ status: "error", error: "Invalid password" });
  // }
  // if (plainTextPassword.length < 5) {
  //   return res.json({
  //     status: "error",
  //     error: "Password too small. Should be atleast 6 characters",
  //   });
  // }
  // const password = await bcrypt.hash(plainTextPassword, 10);

  // ไม่ปรับ Password
  const { username, password } = req.body;

  if (!username || typeof username !== "string") {
    return res.json({ status: "error", error: "Invalid username" });
  }

  if (!password || typeof password !== "string") {
    return res.json({ status: "error", error: "Invalid password" });
  }

  if (password.length < 5) {
    return res.json({
      status: "error",
      error: "Password too small. Should be atleast 6 characters",
    });
  }
  try {
    const response = await User.create({
      username,
      password,
    });
    console.log("User created successfully: ", response);
  } catch (error) {
    if (error.code === 11000) {
      // duplicate key
      return res.json({ status: "error", error: "Username already in use" });
    }
    throw error;
  }
  res.json({ ststus: "ok" });
});

function verifyToken(req, res, next) {
  // Get auth header value
  const bearerHeader = req.headers["auth-token"];
  // const bearerHeader = req.headers["authorization"];

  // Check if bearer is undefined
  if (typeof bearerHeader !== "undefined") {
    // Split at the space
    const bearer = bearerHeader.split(" ");
    // Get token from array
    const bearerToken = bearer[1];
    // Set the token
    req.token = bearerToken;
    // Next middleware
    next();
  } else {
    // Forbidden
    res.sendStatus(403);
  }
}
module.exports = router;
