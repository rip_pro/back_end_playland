const express = require("express");
const { findById } = require("../models/post");
const router = express.Router();
const Post = require("../models/post"); //import Post models

// router.get("/", (req, res) => {
//   res.send("Weare on posts");
// });

//GET BLACK ALL THE POSTS
router.get("/", async (req, res) => {
  try {
    const posts = await Post.find();
    res.json(posts);
  } catch (err) {
    res.json({ message_posts_get: err });
  }
});

//SUBMITS A POSTS
router.post("/", async (req, res) => {
  const { title, description, date } = req.body;
  const post = new Post({
    title,
    description,
    date,
  });
  try {
    const savedPost = await post.save();
    res.json(savedPost);
  } catch (error) {
    res.json({ message_posts_post: err }); // เมื่อ postผิดพลาดจะแสดง message err
  }
});

//SPECIFIC POST
router.get("/:postId", async (req, res) => {
  try {
    // console.log(req.params.postId);
    const post = await Post.findById(req.params.postId);
    res.json(post);
  } catch (error) {
    res.json({ message_posts_postId: err });
    // console.log(res.json({ message_posts_postId: err }));
  }
});

//DELETE POST
router.delete("/:postId", async (req, res) => {
  try {
    const removedPost = await Post.remove({
      _id: req.params.postId,
    });
    res.json(removedPost);
  } catch (error) {
    res.json({ message_posts_delete: err });
  }
});

//UPDATE A POST ซึ่งฟังชั่นอัพเดทจะแก้ไขเฉพาะบางตัว "title"
router.patch("/:postId", async (req, res) => {
  try {
    const updatePost = await Post.updateOne(
      { _id: req.params.postId },
      { $set: { title: req.body.title } }
    );
    res.json(updatePost);
  } catch (error) {
    res.json({ message_posts_patsh: err });
  }
});
module.exports = router;
