const express = require("express");
const router = express.Router();
const Budget = require("../models/budget"); //import Post models

router.get("/", async (req, res) => {
  try {
    const get = await Budget.find();
    res.json(get);
  } catch (err) {
    res.json({ message_posts_get: err });
  }
});

router.get("/:postId", async (req, res) => {
  try {
    // console.log(req.params.postId);
    const post = await Budget.findById(req.params.postId);
    res.json(post);
  } catch (error) {
    res.json({ message_posts_postId: err });
    // console.log(res.json({ message_posts_postId: err }));
  }
});

router.post("/", async (req, res) => {
  const iterable = req.body.budgettwo[0].tabel_data;
  const post = new Budget({
    budgetone: [
      {
        id_file: req.body.budgetone[0].id_file,
        date_file: req.body.budgetone[0].date_file,
        presenter: req.body.budgetone[0].presenter,
        affiliation: req.body.budgetone[0].affiliation,
        offerprice: req.body.budgetone[0].offerprice,
        necessity: req.body.budgetone[0].necessity,
        action: req.body.budgetone[0].action,
        date_start: req.body.budgetone[0].date_start,
        date_finished: req.body.budgetone[0].date_finished,
        sum_total: req.body.budgetone[0].sum_total,
        sum_tatal_text: req.body.budgetone[0].sum_tatal_text,
        date_in: req.body.budgetone[0].date_in,
        time_in: req.body.budgetone[0].time_in,
        sing_presenter: req.body.budgetone[0].sing_presenter,
        confirm_presenter: req.body.budgetone[0].confirm_presenter,
        date_presenter: req.body.budgetone[0].date_presenter,
        sing_head: req.body.budgetone[0].sing_head,
        confirm_head: req.body.budgetone[0].confirm_head,
        date_head: req.body.budgetone[0].date_head,
      },
    ],
    budgettwo: [
      {
        id_file: req.body.budgettwo[0].id_file,
        date_file: req.body.budgettwo[0].date_file,
        presenter: req.body.budgettwo[0].presenter,
        affiliation: req.body.budgettwo[0].affiliation,
        total: req.body.budgettwo[0].total,
        discount: req.body.budgettwo[0].discount,
        discount_price: req.body.budgettwo[0].discount_price,
        vat: req.body.budgettwo[0].vat,
        sum_txt_total: req.body.budgettwo[0].sum_txt_total,
        sum_total: req.body.budgettwo[0].sum_total,
        note: req.body.budgettwo[0].note,
        purchasing: req.body.budgettwo[0].purchasing,
        condition: req.body.budgettwo[0].condition,
        payment: req.body.budgettwo[0].payment,
        bangkok: req.body.budgettwo[0].bangkok,
        number_bangkok: req.body.budgettwo[0].number_bangkok,
        name_bangkok: req.body.budgettwo[0].name_bangkok,
        tabel_data: iterable,
      },
    ],
    budgetthree: [
      {
        purchase_radio: [
          {
            purchase_id: req.body.budgetthree[0].purchase_radio[0].purchase_id,
            purchase_date:
              req.body.budgetthree[0].purchase_radio[0].purchase_date,
            purchase_time:
              req.body.budgetthree[0].purchase_radio[0].purchase_time,
            purchase_txt:
              req.body.budgetthree[0].purchase_radio[0].purchase_txt,
          },
        ],
        purchase_sing: req.body.budgetthree[0].purchase_sing,
        purchase_date: req.body.budgetthree[0].purchase_date,
        manage_radio: [
          {
            manage_id: req.body.budgetthree[0].manage_radio[0].manage_id,
            manage_date: req.body.budgetthree[0].manage_radio[0].manage_date,
            manage_time: req.body.budgetthree[0].manage_radio[0].manage_time,
            manage_txt: req.body.budgetthree[0].manage_radio[0].manage_txt,
          },
        ],
        manage_sing: req.body.budgetthree[0].manage_sing,
        manage_date: req.body.budgetthree[0].manage_sing,
        account_radio: [
          {
            radio_id: req.body.budgetthree[0].account_radio[0].radio_id,
            cashier: req.body.budgetthree[0].account_radio[0].cashier,
            numberaccount:
              req.body.budgetthree[0].account_radio[0].numberaccount,
            confirm_account:
              req.body.budgetthree[0].account_radio[0].confirm_account,
            cashier_date: req.body.budgetthree[0].account_radio[0].cashier_date,
            bangkok: req.body.budgetthree[0].account_radio[0].bangkok,
            number_bangkok:
              req.body.budgetthree[0].account_radio[0].number_bangkok,
            name_bangkok: req.body.budgetthree[0].account_radio[0].name_bangkok,
          },
        ],
        account_payment: req.body.budgetthree[0].account_payment,
        total_money: req.body.budgetthree[0].total_money,
        total_moneytext: req.body.budgetthree[0].total_moneytext,
        note: req.body.budgetthree[0].note,
        date_pay: req.body.budgetthree[0].date_pay,
        time_pay: req.body.budgetthree[0].time_pay,
        person_pay: req.body.budgetthree[0].person_pay,
        person_receive: req.body.budgetthree[0].person_receive,
        approve_day: req.body.budgetthree[0].approve_day,
        approve_time: req.body.budgetthree[0].approve_time,
        slip_radio: [
          {
            radio_id: req.body.budgetthree[0].slip_radio[0].radio_id,
            total: req.body.budgetthree[0].slip_radio[0].total,
            txt_total: req.body.budgetthree[0].slip_radio[0].txt_total,
          },
        ],
        slip_date: req.body.budgetthree[0].slip_date,
        slipperson_pay: req.body.budgetthree[0].slipperson_pay,
        slipperson_receive1: req.body.budgetthree[0].slipperson_receive1,
        slipapprove_day: req.body.budgetthree[0].slipapprove_day,
        slipapprove_time: req.body.budgetthree[0].slipapprove_time,
        slipperson_receive2: req.body.budgetthree[0].slipperson_receive2,
      },
    ],
  });
  try {
    const save = await post.save();
    res.json(save).status(200);
  } catch (error) {
    res.json({ message: err }); // เมื่อ postผิดพลาดจะแสดง message err
  }
});
module.exports = router;
