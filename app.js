const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
// set up our express app
const cors = require("cors");
const app = express();
require("dotenv/config");
app.use(bodyParser.json()); //เอาไว้อ่านข้อมูลคามที่ถูกส่องเข้ามาโดยผ่านbody
app.use(cors({ origin: true }));
//Import Routes
const postsRoute = require("./routes/posts");
const budgetsRoute = require("./routes/budgets");
const specificsRoute = require("./routes/specifics");
const supportsRoute = require("./routes/supports");
const usersRoute = require("./routes/users");

app.use("/api/v1/budget", budgetsRoute);
app.use("/api/v1/specific", specificsRoute);
app.use("/api/v1/support", supportsRoute);
app.use("/posts", postsRoute);
app.use("/api/v1/user", usersRoute);

// connect to mongodb
// process.env.DB_CONNECTION;  เป็นการชี้ไปยังที่อยู่ของ link DB
mongoose.connect(
  // process.env.DB_TEST,
  process.env.DB_CONNECTION_TEST,
  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true },
  () => console.log("MongoDB connected!")
);

// listen for requests
app.listen(process.env.port || 5000, function () {
  console.log(`Ready to Go!`);
});
