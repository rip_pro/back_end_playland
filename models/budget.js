const mongoose = require("mongoose");

const BugetSchema = mongoose.Schema({
  budgetone: [
    {
      id_file: {
        type: String,
      },
      date_file: {
        type: String,
      },
      presenter: {
        type: String,
      },
      affiliation: {
        type: String,
      },
      offerprice: {
        type: String,
      },
      necessity: {
        type: String,
      },
      action: {
        type: String,
      },
      date_start: {
        type: String,
      },
      date_finished: {
        type: String,
      },
      sum_total: {
        type: String,
      },
      sum_tatal_text: {
        type: String,
      },
      date_in: {
        type: String,
      },
      time_in: {
        type: String,
      },
      sing_presenter: {
        type: String,
      },
      confirm_presenter: {
        type: String,
      },
      date_presenter: {
        type: String,
      },
      sing_head: {
        type: String,
      },
      confirm_head: {
        type: String,
      },
      date_head: {
        type: String,
      },
    },
  ],
  budgettwo: [
    {
      id_file: { type: String },
      date_file: { type: String },
      presenter: { type: String },
      affiliation: { type: String },
      tabel_data: [
        {
          data_id: { type: String },
          data_list: { type: String },
          data_total: { type: String },
          data_unit: { type: String },
          data_unit_price: { type: String },
        },
      ],
      total: { type: String },
      discount: { type: String },
      discount_price: { type: String },
      vat: { type: String },
      sum_txt_total: { type: String },
      sum_total: { type: String },
      note: { type: String },
      purchasing: { type: String },
      condition: { type: String },
      payment: { type: String },
      bangkok: { type: String },
      number_bangkok: { type: String },
      name_bangkok: { type: String },
    },
  ],
  budgetthree: [
    {
      purchase_radio: [
        {
          purchase_id: {
            type: String,
          },
          purchase_date: {
            type: String,
          },
          purchase_time: {
            type: String,
          },
          purchase_txt: {
            type: String,
          },
        },
      ],
      purchase_sing: {
        type: String,
      },
      purchase_date: {
        type: String,
      },
      manage_radio: [
        {
          manage_id: {
            type: String,
          },
          manage_date: {
            type: String,
          },
          manage_time: {
            type: String,
          },
          manage_txt: {
            type: String,
          },
        },
      ],
      manage_sing: {
        type: String,
      },
      manage_date: {
        type: String,
      },
      account_radio: [
        {
          radio_id: {
            type: String,
          },
          cashier: {
            type: String,
          },
          numberaccount: {
            type: String,
          },
          confirm_account: {
            type: String,
          },
          cashier_date: {
            type: String,
          },
          bangkok: {
            type: String,
          },
          number_bangkok: {
            type: String,
          },
          name_bangkok: {
            type: String,
          },
        },
      ],
      account_payment: {
        type: String,
      },
      total_money: {
        type: String,
      },
      total_moneytext: {
        type: String,
      },
      note: {
        type: String,
      },
      date_pay: {
        type: String,
      },
      time_pay: {
        type: String,
      },
      person_pay: {
        type: String,
      },
      person_receive: {
        type: String,
      },
      approve_day: {
        type: String,
      },
      approve_time: {
        type: String,
      },
      slip_radio: [
        {
          radio_id: {
            type: String,
          },
          total: {
            type: String,
          },
          txt_total: {
            type: String,
          },
        },
      ],
      slip_date: {
        type: String,
      },
      slipperson_pay: {
        type: String,
      },
      slipperson_receive1: {
        type: String,
      },
      slipapprove_day: {
        type: String,
      },
      slipapprove_time: {
        type: String,
      },
      slipperson_receive2: {
        type: String,
      },
    },
  ],
});

module.exports = mongoose.model("budget", BugetSchema);
