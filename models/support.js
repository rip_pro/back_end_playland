const mongoose = require("mongoose");

const SpecificSchema = mongoose.Schema({
  supportone: [
    {
      id_file: { type: String },
      date_file: { type: String },
      moneysupport: { type: String },
      supportpeople: { type: String },
      necessity: { type: String },
      goal_project: { type: String },
      manage: { type: String },
      phase_manage: { type: String },
      details: { type: String },
      consider_media: [
        {
          media: [
            {
              media_check: { type: String },
              media_date: { type: String },
              media_since: { type: String },
            },
          ],
          sing_presenter: { type: String },
          date_presenter: { type: String },
        },
      ],
      budget: [
        {
          budget: [
            {
              total: { type: String },
              txt_total: { type: String },
            },
          ],
          borrow_budget: [
            {
              setback: { type: String },
              total: { type: String },
              txt_total: { type: String },
            },
          ],
          my_support: [
            {
              total: { type: String },
              txt_total: { type: String },
            },
          ],
          sum_total: { type: String },
          sum_txt_total: { type: String },
        },
      ],
      result_project: { type: String },
      sing_presenter: { type: String },
      confirm_presenter: { type: String },
      date_presenter: { type: String },
      support_money_day: { type: String },
      support_money_time: { type: String },
      consider: [
        {
          consider: [
            {
              consider_check: { type: String },
              consider_date: { type: String },
              consider_since: { type: String },
            },
          ],
          sing_presenter: { type: String },
          confirm_presenter: { type: String },
          date_presenter: { type: String },
        },
      ],
      monney: [
        {
          date_payee: { type: String },
          time_payee: { type: String },
          money: [
            {
              money_check: { type: String },
              money_bankok: { type: String },
              name_bankok: { type: String },
              number_bankok: { type: String },
            },
          ],
          sum_total: { type: String },
          sum_txt_total: { type: String },
          note: { type: String },
          sing_payer: { type: String },
          sing_payee: { type: String },
          approve_date: { type: String },
        },
      ],
    },
  ],
  supporttwo: [
    {
      nameproject: { type: String },
      tabel_data: [
        {
          data_id: { type: String },
          data_list_price: { type: String },
          data_price: { type: String },
        },
      ],
      total: { type: String },
      txt_total: { type: String },
    },
  ],
});

module.exports = mongoose.model("support", SpecificSchema);

// const Blogs = (module.exports = mongoose.model("blogs", blogSchema));

// module.exports.getAllposts = function (data) {
//   Posts.find(data);
// };
