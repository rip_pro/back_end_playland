const mongoose = require("mongoose");

const SpecificSchema = mongoose.Schema({
  specificone: [
    {
      id_file: {
        type: String,
      },
      date_file: {
        type: String,
      },
      presenter: {
        type: String,
      },
      affiliation: {
        type: String,
      },
      clarify_information: {
        type: String,
      },
      total: {
        type: String,
      },
      txt_total: {
        type: String,
      },
      date_start: {
        type: String,
      },
      date_stop: {
        type: String,
      },
      sum_total: {
        type: String,
      },
      sum_total_txt: {
        type: String,
      },
      offer_date: {
        type: String,
      },
      offer_time: {
        type: String,
      },
      sing_presenter: {
        type: String,
      },
      confirm_presenter: {
        type: String,
      },
      date_presenter: {
        type: String,
      },
      examine: [
        {
          examine_file: [
            {
              file_check: {
                type: String,
              },
              file_date: {
                type: String,
              },
              file_editfile: {
                type: String,
              },
              file_txt_total: {
                type: String,
              },
            },
          ],
          // txt_total: {
          //   type: String,
          // },
          note: {
            type: String,
          },
          sing_inspector: {
            type: String,
          },
          date_inspector: {
            type: String,
          },
          receive_money: {
            type: String,
          },
          receive_money_txt: {
            type: String,
          },
          money: [
            {
              money_check: {
                type: String,
              },
              money_bankok: {
                type: String,
              },
              name_bankok: {
                type: String,
              },
              number_bankok: {
                type: String,
              },
            },
          ],
          money_note: {
            type: String,
          },
          money_day: {
            type: String,
          },
          money_time: {
            type: String,
          },
          sing_money_inspector: {
            type: String,
          },
          date_money: {
            type: String,
          },
          sing_payee: {
            type: String,
          },
        },
      ],
    },
  ],
  specifictwo: [
    {
      id_file: {
        type: String,
      },
      date_file: {
        type: String,
      },
      presenter: {
        type: String,
      },
      affiliation: {
        type: String,
      },
      tabel_data: [
        {
          data_id: { type: String },
          data_list: { type: String },
          data_total: { type: String },
          data_unit: { type: String },
          data_unit_price: { type: String },
          data_sum_total: { type: String },
        },
      ],
      total: {
        type: String,
      },
      txt_total: {
        type: String,
      },
    },
  ],
});

module.exports = mongoose.model("specifics", SpecificSchema);

// const Blogs = (module.exports = mongoose.model("blogs", blogSchema));

// module.exports.getAllposts = function (data) {
//   Posts.find(data);
// };
