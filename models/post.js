const mongoose = require("mongoose");

const PostSchema = mongoose.Schema({
  title: {
    type: String,
  },
  description: {
    type: String,
  },
  date: {
    type: String,
    // required: true,
  },
});

module.exports = mongoose.model("posts", PostSchema);
